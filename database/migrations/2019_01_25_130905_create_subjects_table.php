<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 100)->unique();
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('excerpt', 300)->nullable();
            $table->string('status', 30)->default('draft')->nullable();
            $table->longText('content')->nullable();
            $table->integer('children')->nullable()->default(0);
            $table->timestamps();

            // Foreign Key recursive
            $table->foreign('parent_id')->references('id')->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
