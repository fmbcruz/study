<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subject_id');
            $table->string('author', 100)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('url', 200);
            $table->string('ip', 100);
            $table->timestamp('date')->useCurrent();
            $table->text('comment');
            $table->boolean('approved')->default(0);
            $table->unsignedInteger('parent_id');
            $table->unsignedInteger('user_id');
            $table->integer('likes');

            // Foreign keys
            $table->foreign('parent_id')->references('id')->on('comments');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
