<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sample', 'SampleController@index')->name('sample');


Route::middleware('auth')->prefix('admin')->namespace('admin')->group(function(){

    Route::resource('usuarios', 'UserController');
    Route::resource('bancas', 'ApplicatorController');
    Route::resource('comentarios', 'UserController');
    Route::resource('simulados', 'UserController');
    Route::resource('instituicoes', 'InstitutionController');
    Route::resource('notas', 'UserController');
    Route::resource('cargos', 'OccupationController');
    Route::resource('opcoes', 'UserController');
    Route::resource('questoes', 'QuestionController');
    Route::resource('slugs', 'UserController');
    Route::resource('tags', 'UserController');
    Route::resource('usermeta', 'UserController');
});