@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <breadcrumb></breadcrumb>
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <h4>Editor de texto</h4>
                    <text-editor id="editor-text"></text-editor>

                    <gap size="3"></gap>

                    <h4>Upload de imagem</h4>
                    <input-upload name="imagem"></input-upload>

                    <gap size="3"></gap>

                    <h4>Input Type</h4>
                    <div class="row">
                        <form-head id="formExample" css="col-md-12 form" action="#" method="put" enctype="" token="{{ csrf_token() }}">
                            <div class="row">
                                <input-type size="4" name="inputText" type="text" label="Text"></input-type>
                                <input-type size="6" name="inputNumber" type="number" label="Number"></input-type>
                                <input-type size="2" name="inputPassword" type="password" label="Password"></input-type>
                            </div>
                        </form-head>
                    </div>
                    <div class="row">
                        <input-type size="12" name="inputFullWidht" type="text" label="Full size"></input-type>
                    </div>

                    <gap size="2"></gap>


                    <h4>Campo Notas</h4>
                    <input-note></input-note>

                    <gap size="3"></gap>

                    <h4>Campo Tag</h4>
                    <input-tag></input-tag>

                    <gap size="3"></gap>
                    <h4>Questões</h4>
                    <question></question>

                    <gap size="3"></gap>
                    <h4>Card de Simulado</h4>
                    <div class="row">
                        <exam-card size="3"></exam-card>
                        <exam-card size="3"></exam-card>
                        <exam-card size="3"></exam-card>
                    </div>

                    <gap size="3"></gap>
                    <h4>Card de Matérias</h4>
                    <div class="row">
                        <subject-card size="3"></subject-card>
                        <subject-card size="3"></subject-card>
                        <subject-card size="3"></subject-card>
                    </div>

                    <gap size="3"></gap>
                    <h4>Campo de Busca</h4>
                    <div class="container">
                        <div class="row">
                            <input-search
                                    type="Exemplos"
                                    :list="{{ json_encode([['question' =>'Belo Horizonte'], ['question' =>'Sabará'], ['question' =>'Contagem'], ['question' =>'Ibirité']]) }}"></input-search>
                        </div>
                    </div>

                    <gap size="3"></gap>
                    <h4>Alerta</h4>
                    <alert type="warning" message="Lorem ipsum dolor sit amet, consectetur adipiscing elit."></alert>
                    <alert type="success" message="Lorem ipsum dolor sit amet, consectetur adipiscing elit."></alert>
                    <alert type="danger" message="Lorem ipsum dolor sit amet, consectetur adipiscing elit."></alert>

                    <gap size="3"></gap>
                    <h4>Tabela</h4>
                    <table-field
                            name="tableUsers"
                            :titles="{{ "['#', 'Questão', 'Ano', 'Usado', 'Opções']" }}"
                    ></table-field>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection