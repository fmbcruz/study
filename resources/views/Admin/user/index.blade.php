@extends('layouts.app')

@php
    $option = json_encode([['value' => 1, 'name' => 'Ativado'], ['value' => 0, 'name' => 'Desativado']]);
    $url = route('usuarios.index');
    $name = 'usuário';
@endphp

@section('content')
    <div class="container">
        <div class="row center-mobile">
            <breadcrumb :list="{{ $breadcrumbs }}"></breadcrumb>
        </div>
        <div class="row justify-content-center">
            <h2>Usuários</h2>
        </div>
        <div class="row table-options">
            <div class="col-md-2 mb-4 col-sm-12">
                <button id="add" class="btn btn-success full-width-mobile" data-toggle="modal" data-target="#modalDefault">
                    <i class="fas fa-plus-circle"></i> Adicionar
                </button>
            </div>
            <div class="col-md-4 col-sm-12 offset-md-6 float-right justify-content-sm-center">
                <form-head id="formSearch" css="" action="{{ $url }}" method="get" enctype="" token="">
                    <input-search type="{{ $name }}" :list="{{ $suggest }}" term="{{ request()->busca }}"></input-search>
                </form-head>
            </div>
        </div>
        <div class="row">
            <table-field
                    name="tableUsers"
                    :titles="['#', 'Nome', 'E-mail', 'Status', 'Opções']"
                    :content="{{ json_encode($collection) }}"
                    total="{{ $collection->total() }}"
            > {{ $collection->links() }} </table-field>
        </div>
        <modal size="lg" title="Editar {{ $name }}" editurl="{{ $url }}" secondtitle="Cadastrar {{ $name }}">
            <div class="container">
                <form-head id="formDefault" css="" action="" method="put" enctype="" token="{{ csrf_token() }}">
                    <div class="row">
                        <input-type name="name" type="text" label="Nome"></input-type>
                        <select-type
                                name="status"
                                size= "3"
                                label="status"
                                items="{{ $option }}"></select-type>
                    </div>
                    <div class="row">
                        <input-type size="6" name="email" type="email" label="E-mail"></input-type>
                        <input-type size="6" name="password" type="password" label="Senha"></input-type>
                    </div>
                </form-head>
            </div>
        </modal>
        <modal-delete size="" title="Excluir de {{ $name }}" editurl="{{ $url }}">
            <form-head id="defaultDelete" css="" action="" method="delete" enctype="" token="{{ csrf_token() }}"></form-head>
            <p>Tem certeza que deseja excluir a {{ $name }}?</p>
            <p>Esta ação não poderá ser revertida!</p>
        </modal-delete>
    </div>
@endsection