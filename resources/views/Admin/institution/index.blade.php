@extends('layouts.app')

@php
    $url = route('instituicoes.index');
    $name = 'instituição';
@endphp

@section('content')
    <div class="container">
        <div class="row center-mobile">
            <breadcrumb :list="{{ $breadcrumbs }}"></breadcrumb>
        </div>
        <div class="row justify-content-center">
            <h2>Instituições</h2>
        </div>
        <div class="row table-options">
            <div class="col-md-2 mb-4 col-sm-12">
                <button id="add" class="btn btn-success full-width-mobile" data-toggle="modal" data-target="#modalDefault">
                    <i class="fas fa-plus-circle"></i> Adicionar
                </button>
            </div>
            <div class="col-md-4 col-sm-12 offset-md-6 float-right justify-content-sm-center">
                <form-head id="formSearch" css="" action="{{ $url }}" method="get" enctype="" token="">
                    <input-search type="{{ $name }}" :list="{{ $suggest }}" term="{{ request()->busca }}"></input-search>
                </form-head>
            </div>
        </div>
        <div class="row">
            <table-field
                    name="tableUsers"
                    :titles="['#', 'Nome', 'Banca', 'Opções']"
                    :content="{{ json_encode($collection) }}"
                    total="{{ $collection->total() }}"
            > {{ $collection->links() }} </table-field>
        </div>
        <modal size="" title="Editar {{ $name }}" editurl="{{ $url }}" secondtitle="Cadastrar {{ $name }}">
            <div class="container">
                <form-head id="formDefault" css="" action="" method="put" enctype="" token="{{ csrf_token() }}">
                    <div class="row">
                        <input-type name="name" type="text" label="Nome"></input-type>
                        <select-type
                                name="applicator_id"
                                size= "4"
                                label="Banca"
                                items="{{ $applicators }}">
                        </select-type> <!-- fazer este select atualizar ao editar -->
                    </div>
                </form-head>
            </div>
        </modal>
        <modal-delete size="" title="Excluir de {{ $name }}" editurl="{{ $url }}">
            <form-head id="defaultDelete" css="" action="" method="delete" enctype="" token="{{ csrf_token() }}"></form-head>
            <p>Tem certeza que deseja excluir a {{ $name }}?</p>
            <p>Esta ação não poderá ser revertida!</p>
        </modal-delete>
    </div>
@endsection