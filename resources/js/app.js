
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('text-editor', require('./components/TextEditor.vue').default);
Vue.component('breadcrumb', require('./components/Breadcrumb.vue').default);
Vue.component('alert', require('./components/Alert.vue').default);
Vue.component('modal', require('./components/Modal.vue').default);
Vue.component('modal-delete', require('./components/ModalDelete.vue').default);
Vue.component('gap', require('./components/Gap.vue').default);
Vue.component('table-field', require('./components/Table.vue').default);
Vue.component('question', require('./components/Question.vue').default);
Vue.component('exam-card', require('./components/ExamCard.vue').default);
Vue.component('subject-card', require('./components/SubjectCard.vue').default);
Vue.component('form-head', require('./components/forms/Form.vue').default);
Vue.component('input-upload', require('./components/forms/Upload.vue').default);
Vue.component('input-type', require('./components/forms/Input.vue').default);
Vue.component('select-type', require('./components/forms/Select.vue').default);
Vue.component('input-note', require('./components/forms/Notes.vue').default);
Vue.component('input-tag', require('./components/forms/Tags.vue').default);
Vue.component('input-search', require('./components/forms/Search.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
