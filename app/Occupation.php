<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $fillable = [
        'name',
        'salary',
        'description',
        'institution_id',
    ];

    public $timestamps = false;

    public $hidden = [
        'institution_id',
        'description',
    ];

    public function institution()
    {
        return $this->belongsTo('App\Institution');
    }

}
