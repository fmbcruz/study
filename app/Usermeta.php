<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usermeta extends Model
{
    protected $fillable = [
        'meta_key',
        'meta_value'
    ];

}
