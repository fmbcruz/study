<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Applicator extends Model
{
    protected $fillable = [
        'name',
        'alternatives',
        'description',
    ];

    protected $hidden = [
        'created_at'
    ];

    public $timestamps = false;

    public function comments()
    {
        return $this->hasMany('App\Institution');
    }
}
