<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $fillable = [
        'name',
        'applicator_id'
    ];

    public $timestamps = false;

    public $hidden = [
        'applicator_id'
    ];

    public function applicator()
    {
        return $this->belongsTo('App\Applicator');
    }

    public function occupations()
    {
        return $this->hasMany('App\Occupation');
    }
}
