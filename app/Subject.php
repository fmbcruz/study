<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'subject',
        'except',
        'status',
        'content',
        'children'
    ];

    public function children()
    {
        return $this->hasMany('App\Subject');
    }

    public function notes()
    {
        return $this->hasMany('App\Notes');
    }

    public function slug()
    {
        return $this->morphOne('App\Slug', 'slugable');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question');
    }

}
