<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [
        'name',
        'questions',
        'hits',
    ];

    public function user()
    {
        return $this->belongsTo('App\Post');
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question');
    }


}
