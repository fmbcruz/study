<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    protected $fillable = [
        'name',
        'question_id',
        'alternative',
        'correct',
        'note',
    ];

    protected $hidden = [
        'question_id',
        'created_at'
    ];

    public $timestamps = false;

    public function question()
    {
        return $this->hasMany('App\Question');
    }
}
