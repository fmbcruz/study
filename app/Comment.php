<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'author',
        'email',
        'url',
        'ip',
        'date',
        'comment',
        'approved',
    ];

    public function children()
    {
        return $this->hasMany('App\Comment');
    }

    public function commentable()
    {
        return $this->morphTo();
    }
}
