<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static $page_size = 5;

    public static function search($term, $table='', $search=[], $columns=[])
    {
        $table = DB::table($table);
        foreach ($search as $key => $value) {
            if($key != 0) {
                $table->orWhere($value, 'like', "%". strtolower($term) . "%");
            } else {
                $table->where($value, 'like', "%". strtolower($term) . "%");
            }
        }
        return $table->select($columns)->distinct()->paginate(self::$page_size);
    }

    public static function createOption($items=[]) {
        $option = json_encode([['value' => 1, 'name' => 'Ativado'], ['value' => 0, 'name' => 'Desativado']]);
        $option = [['value' => 0, 'name' => 'Selecione ...']];
        foreach ($items as $item) {
            array_push($option, ['value' => $item->id, 'name' => $item->name]);
        }

        return json_encode($option);

    }
}
