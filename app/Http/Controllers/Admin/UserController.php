<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $data)
    {
        $breadcrumbs = json_encode([
           ['title' => 'Painel', 'url' => route('home'), 'icon' => 'fas fa-tachometer-alt'],
           ['title' => 'Usuários', 'url' => '', 'icon' => 'fas fa-users'],
        ]);

        // Suggest
        $suggest = User::select('name')->where('status', 1)->orderBy('name', 'asc')->get();

        if(empty($data->busca)){
            $collection = User::paginate(self::$page_size);
        } else {
            $collection = User::search($data->busca);
            $collection = self::search($data->busca, 'users', ['name', 'email'], ['id', 'name', 'email', 'status']);

        }

        return view('admin.user.index', compact('collection', 'breadcrumbs', 'suggest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);

        try {
            User::create($data);
            return json_encode(['status' => 1, 'msg' =>'Usuário criado com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);

        try {
            User::find($id)->update($data);
            return json_encode(['status' => 1, 'msg' =>'Usuário atualizado com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            User::find($id)->delete();
            return json_encode(['status' => 1, 'msg' =>'Usuário excluído com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }
}
