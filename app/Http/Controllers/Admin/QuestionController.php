<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Question;
use App\Occupation;
use App\Subject;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $data)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Painel', 'url' => route('home'), 'icon' => 'fas fa-tachometer-alt'],
            ['title' => 'Questões', 'url' => '', 'icon' => 'fas fa-question-circle'],
        ]);

        // Suggest
        $suggest = Question::select('question')->orderBy('question', 'asc')->get();
        $subjects = Subject::select('subject')->orderBy('subject', 'asc')->get();

        if(empty($data->busca)){
            $collection = Question::select('id', 'question', 'year', 'used')->paginate(self::$page_size);
        } else {
            $collection = self::search($data->busca, 'questions', ['question'], ['id', 'name']);
        }

//        return $collection;
//
//        foreach ($collection as $quest) {
//                return $quest->occupation->institution;
//        }

        $occupations = self::createOption(Occupation::select('id', 'name')->orderBy('name', 'asc')->get());

        return view('admin.question.index', compact('collection', 'breadcrumbs', 'suggest', 'occupations', 'subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Question::create($request->all());
            return json_encode(['status' => 1, 'msg' =>'Questão criada com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Question::select('id', 'occupation_id', 'question', 'year', 'used', 'note')
            ->with(['occupation', 'subjects'])
            ->where('id', $id)->get();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $question = Question::find($id);
        $array = [];
        foreach ($request->subjects as $subject) {
            $subj = Subject::where('subject', '=', $subject)->first();
            if(!empty($subj->id)) {
                $array[] = $subj->id;
            } else {
                $new = Subject::create(['subject' => $subject]);
                $array[] = $new->id;
            }
        }
        $question->subjects()->sync($array);

        $question->update($request->all());

        try {
            Question::find($id)->update($request->all());
            return json_encode(['status' => 1, 'msg' =>'Questão atualizada com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Question::find($id)->delete();
            return json_encode(['status' => 1, 'msg' =>'Questão excluída com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }
}

