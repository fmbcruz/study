<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Applicator;
use Nexmo\Response;

class ApplicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $data)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Painel', 'url' => route('home'), 'icon' => 'fas fa-tachometer-alt'],
            ['title' => 'Bancas', 'url' => '', 'icon' => 'fas fa-university'],
        ]);

        // Suggest
        $suggest = Applicator::select('name')->orderBy('name', 'asc')->get();

        if(empty($data->busca)){
            $collection = Applicator::paginate(self::$page_size);
        } else {
            $collection = self::search($data->busca, 'applicators', ['name', 'description'], ['id', 'name', 'alternatives']);
        }

        return view('admin.applicator.index', compact('collection', 'breadcrumbs', 'suggest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Applicator::create($request->all());
            return json_encode(['status' => 1, 'msg' =>'Banca criada com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Applicator::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Applicator::find($id)->update($request->all());
            return json_encode(['status' => 1, 'msg' =>'Banca atualizada com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Applicator::find($id)->delete();
            return json_encode(['status' => 1, 'msg' =>'Banca excluída com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }
}
