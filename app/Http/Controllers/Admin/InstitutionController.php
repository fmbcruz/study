<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Institution;
use App\Applicator;

class InstitutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $data)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Painel', 'url' => route('home'), 'icon' => 'fas fa-tachometer-alt'],
            ['title' => 'Instituições', 'url' => '', 'icon' => 'fas fa-building'],
        ]);

        // Suggest
        $suggest = Institution::select('name')->orderBy('name', 'asc')->get();

        if(empty($data->busca)){
            $collection = Institution::with('applicator')->paginate(self::$page_size);
        } else {
            $collection = self::search($data->busca, 'institutions', ['name'], ['id', 'name']);
        }

        $applicators = self::createOption(Applicator::select('id', 'name')->orderBy('name', 'asc')->get());

//        return $collection;

//        foreach ($collection as $key => $item) {
//            $collection['data'] = $item->applicator;
//        }

//        return $collection;

        return view('admin.Institution.index', compact('collection', 'breadcrumbs', 'suggest', 'applicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Institution::create($request->all());
            return json_encode(['status' => 1, 'msg' =>'Instituição criada com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DB::table('institutions')
            ->select('id', 'name', 'applicator_id')
            ->where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Institution::find($id)->update($request->all());
            return json_encode(['status' => 1, 'msg' =>'Instituição atualizada com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Institution::find($id)->delete();
            return json_encode(['status' => 1, 'msg' =>'Instituição excluída com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }
}
