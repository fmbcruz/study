<?php

namespace App\Http\Controllers\Admin;

use App\Institution;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Occupation;
use Illuminate\Support\Facades\DB;

class OccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $data)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Painel', 'url' => route('home'), 'icon' => 'fas fa-tachometer-alt'],
            ['title' => 'Cargos', 'url' => '', 'icon' => 'fas fa-briefcase'],
        ]);

        // Suggest
        $suggest = Occupation::select('name')->orderBy('name', 'asc')->get();

        if(empty($data->busca)){
            $collection = Occupation::with('institution')->paginate(self::$page_size);
        } else {
            $collection = self::search($data->busca, 'occupations', ['name'], ['id', 'name']);
        }

//        return $collection;

        $institutions = self::createOption(Institution::select('id', 'name')->orderBy('name', 'asc')->get());

        return view('admin.occupation.index', compact('collection', 'breadcrumbs', 'suggest', 'institutions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Occupation::create($request->all());
            return json_encode(['status' => 1, 'msg' =>'Cargo criado com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DB::table('occupations')
            ->select('id', 'name', 'institution_id', 'salary', 'description')
            ->where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Occupation::find($id)->update($request->all());
            return json_encode(['status' => 1, 'msg' =>'Cargo atualizado com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Occupation::find($id)->delete();
            return json_encode(['status' => 1, 'msg' =>'Cargo excluído com sucesso!']);
        } catch (\HttpQueryStringException $e) {
            return json_encode(['status' => 0, 'msg' =>$e->getMessage()]);
        }
    }
}
