<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question',
        'year',
        'used',
        'note',
        'occupation_id',
    ];

    public $hidden = [
        'occupation_id',
    ];

    public $timestamps = false;

    public function slug()
    {
        return $this->morphOne('App\Slug', 'slugable');
    }

    public function occupation()
    {
        return $this->belongsTo('App\Occupation');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function alternatives()
    {
        return $this->hasMany('App\Alternative');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject');
    }


}
