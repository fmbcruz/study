<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slug extends Model
{
    protected $fillable = [
        'slug'
    ];

    public function slugable()
    {
        return $this->morphTo();
    }
}
