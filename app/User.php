<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'email_verified_at'
    ];

    public function usermetas()
    {
        return $this->hasMany('App\Usermeta');
    }

    public function exams()
    {
        return $this->hasMany('App\Exam');
    }

    public static function search($term)
    {
        return DB::table('users')
            ->where('name', 'like', "%". strtolower($term) . "%")
            ->orWhere('email', 'like', "%". strtolower($term) . "%")
            ->select('id', 'name','email', 'status')
            ->distinct()
            ->paginate(5);
    }
}
