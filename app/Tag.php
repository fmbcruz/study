<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'tag'
    ];

    public function subjects()
    {
        return $this->morphedByMany('App\Subject', 'taggable');
    }

    public function questions()
    {
        return $this->morphedByMany('App\Question', 'taggable');
    }
}
